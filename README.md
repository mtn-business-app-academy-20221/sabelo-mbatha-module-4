# Sabelo Mbatha Module 4

Exercise to build an animation splash screen with a common theme and implementation of a design onto your app.

## M4-Assessment 2

### Instructions:
* Create a Gitlab account 
* Create a repository on Gitlab as name-surname-module-4

### Assessment:
* Add an animation splash screen.
* Create a common theme for your app, from previous class.
* Implement design onto your app.

#### Disclaimer:
* Used lottie package

#### END!
